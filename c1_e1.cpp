//Fabricio Loor 
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <iostream>

using namespace std;

#define cast(x) if(x==0) cout<<"-"; else if(x==1)cout<<"X";else cout<<"O";

#define cout_mat(x) cout << "| "; cast(x[0][0]); cout<<" | "; cast(x[0][1]); cout<< " | "; cast(x[0][2]) cout << " |" << endl; \
cout << "| "; cast(x[1][0]); cout<<" | "; cast(x[1][1]); cout<< " | "; cast(x[1][2]) cout << " |" << endl; \
cout << "| "; cast(x[2][0]); cout<<" | "; cast(x[2][1]); cout<< " | "; cast(x[2][2]) cout << " |" << endl; 

int jugar_pvp(int matrix[3][3]);

int jugar_pvc(int matrix[3][3]);

int gano(int matrix[3][3]){
	
    if((matrix[0][0] == 1 && matrix[0][1] == 1 && matrix[0][2] == 1) ||
    	(matrix[1][0] == 1 && matrix[1][1] == 1 && matrix[1][2] == 1)  ||
    	(matrix[2][0] == 1 && matrix[2][1] == 1 && matrix[2][2] == 1)  ||
    	(matrix[0][0] == 1 && matrix[1][1] == 1 && matrix[2][2] == 1)  ||
    	(matrix[2][0] == 1 && matrix[1][1] == 1 && matrix[0][2] == 1)  ||
    	(matrix[0][0] == 1 && matrix[1][0] == 1 && matrix[2][0] == 1)  ||
    	(matrix[0][1] == 1 && matrix[1][1] == 1 && matrix[2][1] == 1)  ||
    	(matrix[0][2] == 1 && matrix[1][2] == 1 && matrix[2][2] == 1) )
        return 1;
    if((matrix[0][0] == 2 && matrix[0][1] == 2 && matrix[0][2] == 2) ||
    	(matrix[1][0] == 2 && matrix[1][1] == 2 && matrix[1][2] == 2)  ||
    	(matrix[2][0] == 2 && matrix[2][1] == 2 && matrix[2][2] == 2)  ||
    	(matrix[0][0] == 2 && matrix[1][1] == 2 && matrix[2][2] == 2)  ||
    	(matrix[2][0] == 2 && matrix[1][1] == 2 && matrix[0][2] == 2)  ||
    	(matrix[0][0] == 2 && matrix[1][0] == 2 && matrix[2][0] == 2)  ||
    	(matrix[0][1] == 2 && matrix[1][1] == 2 && matrix[2][1] == 2)  ||
    	(matrix[0][2] == 2 && matrix[1][2] == 2 && matrix[2][2] == 2) )
        return 2;
    return 0;
}

int jugar_pvp(int matrix[3][3]){
	uint32_t p,q;

	for (int i = 0; i < 9; ++i)
	{
		do{
			cout<<"Jugador 1 \nx= ";
			cin>>p;
			cout<<"y= ";
			cin>>q;
		}while(p > 2 || p< 0 || q > 2 || q < 0 || matrix[p][q] != 0 );
		matrix[p][q] = 1;
		cout_mat(matrix)
		if(gano(matrix)){
			cout<<"GANO JUGADOR 1"<<endl;
			return 1;
		}
		do{
			cout<<"Jugador 2 \nx= ";
			cin>>p;
			cout<<"y= ";
			cin>>q;
		}while(p > 2 || p< 0 || q > 2 || q < 0 || matrix[p][q] != 0 );

		matrix[p][q] = 2;
		cout_mat(matrix)
		if(gano(matrix)){
			cout<<"GANO JUGADOR 2"<<endl;
			return 1;
		}
	}
	cout<<"EMPATE"<<endl;
	return 1;
}

int jugar_pvc(int matrix[3][3]){
	uint32_t p,q;

	for (int i = 0; i < 9; ++i)
	{
		do{
			cout<<"Jugador\nx";
			cin>>p;
			cout<<"Posicion y";
			cin>>q;
		}while(p > 2 || p< 0 || q > 2 || q < 0 || matrix[p][q] != 0 );
		
		matrix[p][q] = 1;
		cout_mat(matrix)
		if(gano(matrix)){
			cout<<"GANO JUGADOR 1"<<endl;
			return 1;
		}
		
		do{
			p = rand() % 3;
			q = rand() % 3;
		}while(p > 2 || p< 0 || q > 2 || q < 0 || matrix[p][q] != 0 );
		matrix[p][q] = 2;
		cout_mat(matrix)
		if(gano(matrix)){
			cout<<"GANO COMPUTADORA"<<endl;
			return 1;
		}
	}
	cout<<"EMPATE"<<endl;
	return 1;
}

int main(){
	int matrix[3][3] = {{0,0,0},{0,0,0},{0,0,0}};
	
	char st2[] = "Elegir opción\n 1-Jugador contra Jugador\n 2-Jugador contra máquina \n (-1 terminar):  ";
	int dd;	
    std::string nombre1,nombre2;
	
	cout<<st2<<endl;
    cin>>dd;
    while(dd != -1){
        switch(dd){
	        case 1:
	            cout<<"Nombre jugador 1: ";
	            cin>> nombre1;
	            cout<<"Nombre jugador 2: ";
	             cin>> nombre2;
	            jugar_pvp(matrix);
	            break;
	        case 2:
	            cout<<"Nombre jugador 1: ";
	             cin>> nombre1;
	            jugar_pvc(matrix);
	            break;
	        
	        default:
	            cout<<"Opcion incorrecta \n";
        }
        cout<<st2<<"\n";
        cin>>dd;
    }
    return 0;


}