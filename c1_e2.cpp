//Fabricio Loor 
#include <stdio.h>
#include <algorithm>
#include <iostream>
#include <vector>
#include <string>

using namespace std;

int main(){
	int dd;	
	string input;
	cout<<"Input: ";
    cin>>input;
    int esp_blancos = input.size()/2+2;
    for(int j= 0; j < esp_blancos+1; cout<<" ", j++);
    cout<<input[0]<<endl;
    for (int i = 1; i < input.size(); ++i)
    {
        for(int j= 0; j < esp_blancos; cout<<" ", j++);
        cout<<input[i];
        for(int j= 0; j < i*2; cout<<" ", j++);
        cout<<input[i];
        cout<<"\n";
        esp_blancos--;        
    }
    for (int i = input.size()-1; i >= 0; --i)
        cout<<input[i];
    for (int i = 0; i <input.size(); ++i)
        cout<<input[i];
    cout<<endl;
    return 0;
}